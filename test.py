from datetime import timedelta

from mastodon import Mastodon

import autoreject
import secret

masto = Mastodon(
    access_token = secret.ACCESS_TOKEN,
    api_base_url = secret.API_BASE
)

criteria = {
    "min_follows": 10,
    "min_followers": 10,
    "max_follows": 100,
    "max_followers": 100,
    "has_bio": True,
    "has_avatar": True,
    "min_posts": 10,
    "max_posts": 100,
    "min_age": timedelta(days=7),
    "no_bots": True
}

autoreject.reject_follows(masto, criteria)